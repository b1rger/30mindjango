from django.db import models

# Create your models here.

class Book(models.Model):
    # A list of Fieldtypes on https://docs.djangoproject.com/en/2.0/ref/models/fields/
    title = models.CharField(max_length=255)
    author = models.CharField(max_length=255)
    year = models.PositiveSmallIntegerField()
