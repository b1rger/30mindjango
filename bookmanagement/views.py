from django.shortcuts import render

# Create your views here.

from django.views import View
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import DetailView, ListView
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from .models import Book

class BookList(ListView):
    model = Book

class BookCreate(CreateView):
    model = Book
    fields = ['title', 'author', 'year']
    success_url = '/books'

class BookDetail(DetailView):
    model = Book

class BookUpdate(UpdateView):
    model = Book
    fields = ['title', 'author', 'year']
    success_url = '/books'

class BookDelete(DeleteView):
    model = Book
    success_url = '/books'

class SimpleView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request):
        return super().dispatch(request)

    def get(self, request):
        return HttpResponse("I'm a teapot!", status=418)

    def post(self, request):
        return HttpResponse("Going postal!")

    def delete(self, request):
        response = JsonResponse({'deleted':'foobar', 'useragent': request.META['HTTP_USER_AGENT']})
        response['X-Clacks-Overhead'] = "GNU Terry Pratchett"
        return response
