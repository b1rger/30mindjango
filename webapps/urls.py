"""webapps URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from bookmanagement import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('createbook/', views.BookCreate.as_view(), name='createbook'),
    path('book/<int:pk>', views.BookDetail.as_view(), name='bookdetail'),
    path('books/', views.BookList.as_view()),
    path('deletebook/<int:pk>', views.BookDelete.as_view(), name='deletebook'),
    path('updatebook/<int:pk>', views.BookUpdate.as_view(), name='updatebook'),
    path('simpleview/', views.SimpleView.as_view()),
]
